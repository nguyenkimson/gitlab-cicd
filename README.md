# GitLab CI/CD <img src="https://img.shields.io/badge/react-%5E16.13.1-blue?logo=react" alt="react: ^16.13.1" /> <img src="https://img.shields.io/badge/nginx-pass-critical?style=flat&logo=nginx" alt="nginx config" /> <img src="https://img.shields.io/badge/server-Digital Ocean-brightgreen?logo=appveyor" alt="server digital ocean" /> <img src="https://img.shields.io/badge/deploy-pass-yellow?logo=gitlab" alt="deploy success" /> <img src="https://img.shields.io/badge/SSL & DNS-pass-ff69b4?logo=cloudflare" alt="use cloudflare config ssl and dns subdomain" />

This repo introduces some ways to use GitLab CI/CD on a ReactJS application.

### :rocket: Features
| Branch | Status | Description |
|--------|:------:|-------------|
| master |:heavy_check_mark:| This branch developed from dev-2. Using rsync and sync directly nginx configuration in every commit. Building and synchronizing the build folder are executed on GitLab share runner |
| dev-1 |:heavy_check_mark:| Using SSH to access into the server and run an available script on the server to get the latest code from the branch and build application inside the server |
| dev-2 |:heavy_check_mark:| Similar master branch without config nginx |
| dev-3 |:heavy_check_mark:| Using GitLab runner to build docker image and push to GitLab registry. After that, runs the script on the server to get that image and run docker on the server |
| dev-4 || Use gitlab-ci.yml template |

### 🤘 Sponsors:
* Domain: https://freenom.com
* SSL & DNS: https://dash.cloudflare.com
* Server: https://cloud.digitalocean.com/

### 📃 References:
* [My note in Vietnamese](https://www.evernote.com/shard/s457/client/snv?noteGuid=ae426e94-d235-83d4-03f1-111a835a71a0&noteKey=2909ed0c1daade127077e7977155e320&sn=https%3A%2F%2Fwww.evernote.com%2Fshard%2Fs457%2Fsh%2Fae426e94-d235-83d4-03f1-111a835a71a0%2F2909ed0c1daade127077e7977155e320&title=GitLab%2BCI%252FCD)
* [GitLab help](https://gitlab.com/help/ci/yaml/README)
