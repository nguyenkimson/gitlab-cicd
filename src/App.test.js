import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText("Hello my friend. I'm learning CI/CD on master branch");
  expect(linkElement).toBeInTheDocument();
});
