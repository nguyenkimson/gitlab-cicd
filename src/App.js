import React, { Component } from 'react';
import Helmet from 'react-helmet';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Helmet title='You Are Doing Great'>
          <meta property='og:url' content='https://urreact.tk' />
          <meta property='og:type' content='article' />
          <meta property='og:title' content='Welcome to my React App' />
          <meta
            property='og:description'
            content='Incredible things in my page'
          />
          <meta
            property='og:image:secure_url'
            content='https://cdn.hipwallpaper.com/i/30/89/uM5iEO.png'
          />
        </Helmet>
        <div className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h2>Hello my friend. I'm learning CI/CD on master branch</h2>
        </div>
      </div>
    );
  }
}

export default App;
